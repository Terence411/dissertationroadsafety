#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd
# import altair as alt
import numpy as np
# import plotly.graph_objects as go
import plotly.graph_objs as go
# import missingno as mn
import plotly.express as px
# import matplotlib.pyplot as plt
# import seaborn as sns
import dash
from dash import dcc
from dash import html
# from uszipcode import SearchEngine
import re
import dash
from dash import dcc
from dash import html
from dash.dependencies import Input, Output


# In[2]:


# !pip install missingno
# !pip install dash
# !pip install uszipcode
# !pip install python-Levenshtein (Using slow pure-python SequenceMatcher. Install python-Levenshtein to remove this warning)
# !pip install pyinstaller
# !pip install dash-tools


# In[3]:


input_data = pd.read_csv('us_accidents_2020_to_2022.csv')


# In[4]:


print(len(input_data))
print(len(input_data.columns))


# In[5]:


input_data.columns


# In[6]:


# input_data['Month'] = input_data['Month'].apply(lambda x: pd.to_datetime(str(x), format='%m').strftime('%B'))


# In[7]:


input_data.head(5)


# In[8]:


input_data['Year'].value_counts()


# In[9]:


input_data['Severity'].value_counts()


# In[10]:


us_population = pd.read_csv('US Population 2020-2022.csv')
us_population = us_population.sort_values(by=['State'])
us_population = us_population.reset_index(drop = True)
us_population.head(10)


# In[11]:


us_population.columns


# In[12]:


us_miles_trav = pd.read_csv('US Miles Traveled 2020-2022.csv')
us_miles_trav = us_miles_trav.sort_values(by=['Year', 'State'])
us_miles_trav = us_miles_trav.reset_index(drop = True)

us_miles_trav_2020 = us_miles_trav[us_miles_trav['Year'] == 2020]
us_miles_trav_2021 = us_miles_trav[us_miles_trav['Year'] == 2021]
us_miles_trav_2022 = us_miles_trav[us_miles_trav['Year'] == 2022]

us_miles_trav.head(10)


# In[13]:


us_miles_trav.columns


# In[14]:


input_data.columns


# In[15]:


# input_data.dtypes


# In[16]:


input_data['Severity'].value_counts()


# In[17]:


replacement_map = {
    'Rain' : 'Rain', 
    'Haze' : 'Fog', 
    'Fair / Windy' : 'Fair', 
    'Heavy Rain' : 'Rain', 
    'Thunder in the Vicinity' : 'Rain',
       'Mostly Cloudy / Windy' : 'Mostly Cloudy', 
    'T-Storm' : 'Rain', 
    'Cloudy / Windy' : 'Cloudy', 
    'Thunder' : 'Rain',
       'Light Rain with Thunder' : 'Light Rain', 
    'Light Drizzle' : 'Light Rain', 
    'Snow' : 'Snow', 
    'Wintry Mix' : 'Rain & Snow',
       'Partly Cloudy / Windy' : 'Partly Cloudy', 
    'Heavy T-Storm' : 'Rain', 
    'Smoke': 'Fog',
    'Fog' : 'Fog', 
    'Light Snow / Windy' : 'Light Snow',
       'Light Rain / Windy' : 'Light Rain', 
    'Heavy Snow' : 'Snow', 
    'N/A Precipitation' : 'Rain', 
    'Drizzle' : 'Light Rain',
       'Haze / Windy' : 'Fog',
    'Rain / Windy' : 'Rain', 
    'Shallow Fog' : 'Fog', 
    'Snow / Windy' : 'Snow',
       'Light Freezing Rain' : 'Rain & Snow',
    'Heavy Rain / Windy' : 'Rain', 
    'Blowing Snow / Windy' : 'Snow',
       'Patches of Fog' : 'Fog', 
    'Heavy Snow / Windy' : 'Snow', 
    'Heavy T-Storm / Windy' : 'Rain',
       'Showers in the Vicinity' : 'Rain',
    'Mist' : 'Fog', 
    'Fog / Windy' : 'Fog', 
    'T-Storm / Windy' : 'Rain',
       'Blowing Snow' : 'Snow', 
    'Thunder / Windy' : 'Rain', 
    'Light Freezing Drizzle' : 'Rain & Snow',
       'Snow and Sleet' : 'Snow', 
    'Drizzle and Fog' : 'Light Rain', 
    'Wintry Mix / Windy' : 'Rain & Snow',
       'Light Sleet' : 'Light Snow', 
    'Light Drizzle / Windy' : 'Light Rain', 
    'Blowing Dust / Windy' : 'Dust',
       'Freezing Rain' : 'Rain & Snow', 
    'Sleet' : 'Snow', 
    'Heavy Drizzle' : 'Rain', 
    'Blowing Dust' : 'Fog',
       'Light Rain Shower' : 'Light Rain', 
    'Light Snow and Sleet' : 'Snow', 
    'Smoke / Windy' : 'Fog',
       'Light Freezing Rain / Windy' : 'Rain & Snow', 
    'Snow and Sleet / Windy' : 'Rain & Snow', 
    'Small Hail' : 'Light Snow',
       'Light Snow Shower' : 'Light Snow', 
    'Squalls / Windy' : 'Rain & Snow', 
    'Hail' : 'Light Snow', 
    'Widespread Dust' : 'Dust',
       'Light Snow and Sleet / Windy' : 'Light Snow', 
    'Heavy Sleet' : 'Snow',
       'Widespread Dust / Windy' : 'Dust', 
    'Light Snow with Thunder' : 'Light Snow',
       'Snow and Thunder' : 'Snow',
    'Tornado' : 'Rain', 
    'Freezing Drizzle' : 'Rain & Snow',
       'Sand / Dust Whirlwinds' : 'Dust', 
    'Rain Shower' : 'Rain', 
    'Light Sleet / Windy' : 'Light Snow',
       'Thunder and Hail' : 'Rain & Snow', 
    'Freezing Rain / Windy' : 'Rain & Snow', 
    'Drizzle / Windy' : 'Light Rain',
       'Squalls' : 'Rain & Snow',
    'Thunder / Wintry Mix' : 'Rain & Snow', 
    'Shallow Fog / Windy' : 'Fog',
       'Drifting Snow / Windy' : 'Snow', 
    'Patches of Fog / Windy' : 'Fog', 
    'Snow Grains' : 'Snow',
       'Thunder / Wintry Mix / Windy' : 'Rain & Snow', 
    'Sleet / Windy' : 'Snow', 
    'Sleet and Thunder' : 'Rain & Snow',
       'Light Snow Shower / Windy' : 'Light Snow', 
    'Heavy Rain Shower' : 'Rain',
       'Heavy Freezing Drizzle' : 'Rain & Snow', 
    'Partial Fog' : 'Fog', 
    'Funnel Cloud' : 'Mostly Cloudy',
       'Sand / Dust Whirls Nearby' : 'Dust', 
    'Sand / Dust Whirlwinds / Windy' : 'Dust',
       'Heavy Freezing Rain' : 'Rain', 
    'Heavy Sleet and Thunder' : 'Rain & Snow', 
    'Blowing Sand' : 'Dust',
       'Blowing Snow Nearby' : 'Snow', 
    'Thunder and Hail / Windy' : 'Rain & Snow',
       'Snow and Thunder / Windy' : 'Rain & Snow', 
    'Heavy Rain Shower / Windy' : 'Rain', 
    'Sand / Windy' : 'Dust',
       'Heavy Snow with Thunder' : 'Snow', 
    'Drifting Snow' : 'Snow',
    'Mostly Cloudy' : 'Cloudy',
    'Partly Cloudy' : 'Cloudy',
    'Light Rain' : 'Rain',
    'Light Snow' : 'Snow'
}


# In[18]:


input_data['Weather_Condition'] = input_data['Weather_Condition'].replace(replacement_map)


# In[19]:


weather_counts = input_data['Weather_Condition'].value_counts()
weather_counts_perc = 100*input_data['Weather_Condition'].value_counts()/len(input_data)
weather_counts_perc = round(weather_counts_perc, 3)

print(weather_counts)
print(weather_counts_perc)


# In[20]:


pull_gap = 0.1

# Calculate pull for each slice
pull_values = [pull_gap * p / (100 - p) for p in weather_counts_perc.values]
# pull_values = [p for p in weather_counts_perc.values]
pull_values


# In[21]:


hourly_counts = input_data['Hour'].value_counts()
hourly_counts.index


# In[22]:


input_data_sev1 = input_data[input_data['Severity'] == 1]
input_data_sev2 = input_data[input_data['Severity'] == 2]
input_data_sev3 = input_data[input_data['Severity'] == 3]
input_data_sev4 = input_data[input_data['Severity'] == 4]

ip_hourly_accident_counts_sev1 = input_data_sev1['Hour'].value_counts()
ip_hourly_accident_counts_sev2 = input_data_sev2['Hour'].value_counts()
ip_hourly_accident_counts_sev3 = input_data_sev3['Hour'].value_counts()
ip_hourly_accident_counts_sev4 = input_data_sev4['Hour'].value_counts()

ip_monthly_accident_counts_sev1 = input_data_sev1['Month'].value_counts()
ip_monthly_accident_counts_sev2 = input_data_sev2['Month'].value_counts()
ip_monthly_accident_counts_sev3 = input_data_sev3['Month'].value_counts()
ip_monthly_accident_counts_sev4 = input_data_sev4['Month'].value_counts()


# In[ ]:





# In[23]:


situational_data_sev = []

situational_data_sev.append({
    'Amenity' : input_data_sev1['Amenity'].value_counts()[True],
    'Traffic_Signal' : input_data_sev1['Traffic_Signal'].value_counts()[True],
    'Stop' : input_data_sev1['Stop'].value_counts()[True],
    'Give_Way' : input_data_sev1['Give_Way'].value_counts()[True],
})

situational_data_sev.append({
    'Amenity' : input_data_sev2['Amenity'].value_counts()[True],
    'Traffic_Signal' : input_data_sev2['Traffic_Signal'].value_counts()[True],
    'Stop' : input_data_sev2['Stop'].value_counts()[True],
    'Give_Way' : input_data_sev2['Give_Way'].value_counts()[True],
})

situational_data_sev.append({
    'Amenity' : input_data_sev3['Amenity'].value_counts()[True],
    'Traffic_Signal' : input_data_sev3['Traffic_Signal'].value_counts()[True],
    'Stop' : input_data_sev3['Stop'].value_counts()[True],
    'Give_Way' : input_data_sev3['Give_Way'].value_counts()[True],
})

situational_data_sev.append({
    'Amenity' : input_data_sev4['Amenity'].value_counts()[True],
    'Traffic_Signal' : input_data_sev4['Traffic_Signal'].value_counts()[True],
    'Stop' : input_data_sev4['Stop'].value_counts()[True],
    'Give_Way' : input_data_sev4['Give_Way'].value_counts()[True],
})

situational_data_sev_keys = situational_data_sev[0].keys()
print(situational_data_sev_keys)

situational_data_sev_values = {}
for i in situational_data_sev_keys:
    situational_data_sev_values[i] = [j[i] for j in situational_data_sev]

print(situational_data_sev_values)

situational_data_sev_values_perc = []

for i in range(4):
    situational_data_sev_values_perc.append(sum(situational_data_sev[i].values()))
    
print(situational_data_sev_values_perc)

# amenity, give way, junction, roundabout, stop, traffic signal


# In[ ]:





# In[24]:


figure11 = px.bar(x=hourly_counts.index, y=hourly_counts.values)

figure11.update_layout(
    title='#Accidents at Hourly Rate',
    xaxis=dict(
                    title='Hour',
                    # Set your custom xticks here
#                     tickmode='array',
                    tickvals=[i for i in range(24)],  # Positions of the ticks on the x-axis
                    ticktext=[str(i)+':00' for i in range(24)],  # Labels for the ticks on the x-axis
#                     dtick=1
                ),
    yaxis_title='Count',
)


# In[25]:


# Create a Dash app
app = dash.Dash(__name__)

# Define the layout of the app
app.layout = html.Div([
    html.H1("Dissertation Viz System", style={'text-align': 'center'}),
    html.H1("Version 1", style={'text-align': 'center'}),
    
#     Start of Div for Section 1
    html.Div([
        html.H2("Section 1", style={'text-align': 'center'}),
        
#         Start of Div for Section 1, Row 1
        html.Div([
#             S1 - R1 - C1
            dcc.Graph(
                id='chart11', 
                figure=figure11
            ),
            
#             S1 - R1 - C2
            dcc.Graph(
                id='chart12',
                figure={
                    'data': [
                        go.Scatter(x=sorted(ip_hourly_accident_counts_sev1.index), 
                                   y=[x for _,x in sorted(zip(ip_hourly_accident_counts_sev1.index, ip_hourly_accident_counts_sev1.values))],
                                    name='Severity Level 1'),
                        go.Scatter(x=sorted(ip_hourly_accident_counts_sev2.index), 
                                   y=[x for _,x in sorted(zip(ip_hourly_accident_counts_sev2.index, ip_hourly_accident_counts_sev2.values))],
                                    name='Severity Level 2'),
                        go.Scatter(x=sorted(ip_hourly_accident_counts_sev3.index), 
                                   y=[x for _,x in sorted(zip(ip_hourly_accident_counts_sev3.index, ip_hourly_accident_counts_sev3.values))],
                                    name='Severity Level 3'),
                        go.Scatter(x=sorted(ip_hourly_accident_counts_sev4.index), 
                                   y=[x for _,x in sorted(zip(ip_hourly_accident_counts_sev4.index, ip_hourly_accident_counts_sev4.values))],
                                    name='Severity Level 4'),
                        ],
                    'layout': go.Layout(
                        title='#Accidents for Different Severity Levels',
                            xaxis=dict(
                            title='Hour',
#                             tickmode='array',
                            tickvals=[i for i in range(24)],  
                            ticktext=[str(i)+':00' for i in range(24)],  
#                             dtick=1
                            ),
                            yaxis=dict(title='Count')
                    )
                }
            ),
        ], 
        style={'display': 'flex', 'justify-content': 'space-between'}),
#         End of Div for Section 1, Row 1
        
#         Start of Div for Section 1, Row 2
        html.Div([
#             S1 - R2 - C1
            dcc.Graph(
                id='chart13',
                figure={
                    'data': [
                        go.Bar(x=sorted(ip_hourly_accident_counts_sev1.index), 
                                y=[x for _,x in sorted(zip(ip_hourly_accident_counts_sev1.index, ip_hourly_accident_counts_sev1.values))],
                                name='Severity Level 1'),
                        go.Bar(x=sorted(ip_hourly_accident_counts_sev2.index), 
                                y=[x for _,x in sorted(zip(ip_hourly_accident_counts_sev2.index, ip_hourly_accident_counts_sev2.values))],
                                name='Severity Level 2'),
                        go.Bar(x=sorted(ip_hourly_accident_counts_sev3.index), 
                                y=[x for _,x in sorted(zip(ip_hourly_accident_counts_sev3.index, ip_hourly_accident_counts_sev3.values))],
                                name='Severity Level 3'),
                        go.Bar(x=sorted(ip_hourly_accident_counts_sev4.index), 
                                y=[x for _,x in sorted(zip(ip_hourly_accident_counts_sev4.index, ip_hourly_accident_counts_sev4.values))],
                                name='Severity Level 4'),
                        ],
                    'layout': go.Layout(
                        title='#Accidents for Different Severity Levels',
                        xaxis=dict(title='Hour'),
                        yaxis=dict(title='Count'),
                        barmode='stack'
                    )
                }
            )
    
        ], 
        style={'display': 'flex', 'justify-content': 'center'})
#         End of Div for Section 1, Row 2
        
    ], style={'margin': '20px'}),
#     End of Div for Section 1
    
    html.Br(),
    html.Br(),
    html.Br(),
    


    
#     Start of Div for Section 2
    html.Div([        
        html.H2("Section 2", style={'text-align': 'center'}),
        
#         Start of Div for Section 2, Row 1 
        html.Div([
#             S2 - R1 - C1
            dcc.Graph(
                id='chart21',
                figure={
                    'data': [
                        go.Bar(x=[i for i in range(1, 5)], 
                                y=situational_data_sev_values['Amenity'],
                              )
                        ],
                    'layout': go.Layout(
                        title='#Accidents for Different Severity Levels',
                        xaxis=dict(title='Hour'),
                        yaxis=dict(title='Count'),
#                         barmode='stack'
                    )
                }
            ),
            
#             S2 - R1 - C2
            dcc.Graph(
                id='chart22',
                figure={
                    'data': [
                        go.Bar(x=[i for i in range(1, 5)], 
                                y=situational_data_sev_values['Traffic_Signal'],
                              )
                        ],
                    'layout': go.Layout(
                        title='#Accidents for Different Severity Levels',
                        xaxis=dict(title='Hour'),
                        yaxis=dict(title='Count'),
#                         barmode='stack'
                    )
                }
            )
    
        ], 
        style={'display': 'flex', 'justify-content': 'center'}),
#         End of Div for Section 2, Row 1
        
#         Start of Div for Section 2, Row 2
        html.Div([
#             S2 - R2 - C1
            dcc.Graph(
                id='chart23',
                figure={
                    'data': [
                        go.Bar(x=[i for i in range(1, 5)], 
                                y=situational_data_sev_values['Stop'],
                              )
                        ],
                    'layout': go.Layout(
                        title='#Accidents for Different Severity Levels',
                        xaxis=dict(title='Hour'),
                        yaxis=dict(title='Count'),
#                         barmode='stack'
                    )
                }
            ),
            
#             S2 - R2 - C2
            dcc.Graph(
                id='chart24',
                figure={
                    'data': [
                        go.Bar(x=[i for i in range(1, 5)], 
                                y=situational_data_sev_values['Give_Way'],
                              )
                        ],
                    'layout': go.Layout(
                        title='#Accidents for Different Severity Levels',
                        xaxis=dict(title='Hour'),
                        yaxis=dict(title='Count'),
#                         barmode='stack'
                    )
                }
            )
    
        ], 
        style={'display': 'flex', 'justify-content': 'center'}),
#         End of Div for Section 2, Row 2
        
#         Start of Div for Section 2, Row 3
        html.Div([
#             S2 - R3 - C1
            dcc.Graph(
                id='chart25',
                figure={
                    'data': [
                        go.Bar(x=list(situational_data_sev_values.keys()), 
                                y=[situational_data_sev_values[i][0]/sum(situational_data_sev[0].values()) for i in situational_data_sev_keys],
                              )
                        ],
                    'layout': go.Layout(
                        title='#Accidents for Different Severity Levels',
                        xaxis=dict(title='Hour'),
                        yaxis=dict(title='Count'),
#                         barmode='stack'
                    )
                }
            ),
            
#             S2 - R3 - C2
            dcc.Graph(
                id='chart26',
                figure={
                    'data': [
                        go.Bar(x=list(situational_data_sev_values.keys()), 
                                y=[situational_data_sev_values[i][1]/sum(situational_data_sev[1].values()) for i in situational_data_sev_keys],
                              )
                        ],
                    'layout': go.Layout(
                        title='#Accidents for Different Severity Levels',
                        xaxis=dict(title='Hour'),
                        yaxis=dict(title='Count'),
#                         barmode='stack'
                    )
                }
            )
    
        ], 
        style={'display': 'flex', 'justify-content': 'center'}),
#         End of Div for Section 2, Row 3
        
#         Start of Div for Section 2, Row 4
        html.Div([
#             S2 - R4 - C1
            dcc.Graph(
                id='chart27',
                figure={
                    'data': [
                        go.Bar(x=list(situational_data_sev_values.keys()), 
                                y=[situational_data_sev_values[i][2]/sum(situational_data_sev[2].values()) for i in situational_data_sev_keys],
                              )
                        ],
                    'layout': go.Layout(
                        title='#Accidents for Different Severity Levels',
                        xaxis=dict(title='Hour'),
                        yaxis=dict(title='Count'),
#                         barmode='stack'
                    )
                }
            ),
            
#             S2 - R4 - C2
            dcc.Graph(
                id='chart28',
                figure={
                    'data': [
                        go.Bar(x=list(situational_data_sev_values.keys()), 
                                y=[situational_data_sev_values[i][3]/sum(situational_data_sev[3].values()) for i in situational_data_sev_keys],
                              )
                        ],
                    'layout': go.Layout(
                        title='#Accidents for Different Severity Levels',
                        xaxis=dict(title='Hour'),
                        yaxis=dict(title='Count'),
#                         barmode='stack'
                    )
                }
            )
    
        ], 
        style={'display': 'flex', 'justify-content': 'center'}),
#         End of Div for Section 2, Row 4
        
    ], style={'margin': '20px'}),
#     End of Div for Section 2   
    
 
    html.Br(),
    html.Br(),
    html.Br(),
    

#     Start of Div for Section 3
    html.Div([        
        html.H2("Section 3", style={'text-align': 'center'}),
        
#         Start of Div for Section 3, Row 1 
        html.Div([
#             S3 - R1 - C1
            dcc.Graph(
                id='chart31',
                figure={
                    'data': [
                        go.Pie(
                            labels=weather_counts_perc.index,
                            values=weather_counts_perc.values,
                            hole=0.25,  # Set the hole size to create a donut chart effect
                            marker=dict(colors=['#FF5733', '#FFC300', '#C70039', '#900C3F', '#581845']),
                            hoverinfo='label+percent',  # Show label and percentage on hover
                            textinfo='value',  # Show the actual value on the chart
                            textfont=dict(size=15, color='#000000'),  # Customize the text font
                            textposition='inside',  # Position the text inside the chart     (outside looks better imp)
                            insidetextfont=dict(size=15),  # Customize the inside text font
                            showlegend=True,  # Show the legend
#                             name='Legend'  # Specify the name of the legend
                            pull = pull_values
                        )
                    ],
                    'layout': go.Layout(
                        title='#Accidents for Different Weather Conditions',
                        font=dict(size=15, color='#000000'),  # Customize the overall font
                        margin=dict(t=80, b=20),  # Adjust the margins
#                         legend=dict(
#                             x=0.5,  # Set the x position of the legend (0 to 1)
#                             y=1.1,  # Set the y position of the legend (0 to 1)
#                             orientation='h',  # Set the orientation to horizontal
#                         )
                    )
                }
            ),
            
#             S3 - R1 - C2
            dcc.Graph(
                id='chart32',
                figure={
                    'data': [
                        go.Bar(x=weather_counts.index, 
                                y=weather_counts.values,
                              )
                        ],
                    'layout': go.Layout(
                        title='#Accidents for Different Weather Conditions',
                        xaxis=dict(title='Weather Condition'),
                        yaxis=dict(title='Count'),
#                         barmode='stack'
                    )
                }
            )
    
        ], 
        style={'display': 'flex', 'justify-content': 'center'}),
#         End of Div for Section 2, Row 1
   
#         Start of Div for Section 3, Row 2 
        html.Div([
#             S3 - R2 - C1
            dcc.Graph(
                id='chart33',
                figure={
                    'data': [
                        go.Scatter(x=sorted(ip_monthly_accident_counts_sev1.index), 
                                   y=[x for _,x in sorted(zip(ip_monthly_accident_counts_sev1.index, ip_monthly_accident_counts_sev1.values))],
                                    name='Severity Level 1'),
                        go.Scatter(x=sorted(ip_monthly_accident_counts_sev2.index), 
                                   y=[x for _,x in sorted(zip(ip_monthly_accident_counts_sev2.index, ip_monthly_accident_counts_sev2.values))],
                                    name='Severity Level 2'),
                        go.Scatter(x=sorted(ip_monthly_accident_counts_sev3.index), 
                                   y=[x for _,x in sorted(zip(ip_monthly_accident_counts_sev3.index, ip_monthly_accident_counts_sev3.values))],
                                    name='Severity Level 3'),
                        go.Scatter(x=sorted(ip_monthly_accident_counts_sev4.index), 
                                   y=[x for _,x in sorted(zip(ip_monthly_accident_counts_sev4.index, ip_monthly_accident_counts_sev4.values))],
                                    name='Severity Level 4'),
                        ],
                    'layout': go.Layout(
                        title='#Monthly Accidents for Different Severity Levels\n(Count)',
                        xaxis=dict(
                            title='Month',
                            tickvals=[i for i in range(1, 13)],  
#                             ticktext=[str(i)+':00' for i in range(24)],  
#                             dtick=1
                            ),
                        yaxis=dict(title='Count'),
                    )
                }
            ),
            
#             S3 - R2 - C2
            dcc.Graph(
                id='chart34',
                figure={
                    'data': [
                        go.Scatter(x=sorted(ip_monthly_accident_counts_sev1.index), 
                                   y=[100 * ( x / sum(ip_monthly_accident_counts_sev1.values))  for _,x in sorted(zip(ip_monthly_accident_counts_sev1.index, ip_monthly_accident_counts_sev1.values))],
                                    name='Severity Level 1'),
                        go.Scatter(x=sorted(ip_monthly_accident_counts_sev2.index), 
                                   y=[100 * ( x / sum(ip_monthly_accident_counts_sev2.values))  for _,x in sorted(zip(ip_monthly_accident_counts_sev2.index, ip_monthly_accident_counts_sev2.values))],
                                    name='Severity Level 2'),
                        go.Scatter(x=sorted(ip_monthly_accident_counts_sev3.index), 
                                   y=[100 * ( x / sum(ip_monthly_accident_counts_sev2.values))  for _,x in sorted(zip(ip_monthly_accident_counts_sev3.index, ip_monthly_accident_counts_sev3.values))],
                                    name='Severity Level 3'),
                        go.Scatter(x=sorted(ip_monthly_accident_counts_sev4.index), 
                                   y=[100 * ( x / sum(ip_monthly_accident_counts_sev2.values))  for _,x in sorted(zip(ip_monthly_accident_counts_sev4.index, ip_monthly_accident_counts_sev4.values))],
                                    name='Severity Level 4'),
                        ],
                    'layout': go.Layout(
                        title='#Monthly Accidents for Different Severity Levels\n(Percentages)',
                        xaxis=dict(
                            title='Month',
                            tickvals=[i for i in range(1, 13)],  
#                             ticktext=[str(i)+':00' for i in range(24)],  
#                             dtick=1
                            ),
                        yaxis=dict(title='Percentage'),
                    )
                }
            )
        ], 
        style={'display': 'flex', 'justify-content': 'center'}),
#         End of Div for Section 2, Row 2        
        
    ], style={'margin': '20px'}),
#     End of Div for Section 2    
    
    
    
])

if __name__ == '__main__':
    app.run_server(debug=True, port=8051)


# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:


#     .....Previous Main Divs
#     html.Div([
#         html.H2("Row XX", style={'text-align': 'center'}),
        
#         html.Div([
#             dcc.Graph(
#             id='chart id 1',
#             figure=figure_to_create),
            
#             dcc.Graph(
#             id='chart id 2',
#             figure=figure_to_create)
            
#         ]), style={'display': 'flex', 'justify-content': 'space-between'}),
        
#         html.Div([
#             dcc.Graph(
#             id='chart id 1',
#             figure=figure_to_create),
            
#             dcc.Graph(
#             id='chart id 2',
#             figure=figure_to_create)
            
#             dcc.Graph(
#             id='chart id 2',
#             figure=figure_to_create)
            
#         ]), style={'display': 'flex', 'justify-content': 'space-between'}),
        
#     ], style={'margin': '20px'}),
    
# #     .....Other Main Divs


# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:


# import dash
# from dash.dependencies import Input, Output, State
# import dash_core_components as dcc
# import dash_html_components as html
# import plotly.graph_objs as go
# import threading

# # Sample data for the first chart
# x_values1 = [1, 2, 3, 4, 5]
# y_values1 = [10, 20, 15, 30, 25]

# # Sample data for the second chart
# x_values2 = [1, 2, 3, 4, 5]
# y_values2 = [5, 15, 10, 25, 20]

# def create_dash_app1():
#     app = dash.Dash(__name__)
    
#     @app.callback(Output('url', 'pathname'),
#                   Input('btn-switch', 'n_clicks'))
#     def switch_dashboard(n_clicks):
#         print('dashboard 1')
#         print(n_clicks)
#         if n_clicks is not None and n_clicks > 0:
#             return '/dashboard2'
# #             return 'http://127.0.0.1:8056/'

#     app.layout = html.Div([
#         html.H1('Dashboard 1'),
#         dcc.Graph(
#             id='chart1',
#             figure={
#                 'data': [
#                     go.Scatter(x=x_values1, y=y_values1, mode='lines+markers', name='Data 1')
#                 ],
#                 'layout': go.Layout(
#                     title='Chart 1',
#                     xaxis=dict(title='X-axis Title'),
#                     yaxis=dict(title='Y-axis Title')
#                 )
#             }
#         ),
#         dcc.Location(id='url'),
#         html.Button('Switch to Dashboard 2', id='btn-switch', n_clicks=0)
#     ])

#     app.run_server(debug=False, port=8055)

# def create_dash_app2():
#     app = dash.Dash(__name__)
    
#     @app.callback(Output('url', 'pathname'),
#                   Input('btn-switch', 'n_clicks'))
#     def switch_dashboard(n_clicks):
#         print('dashboard 2')
#         print(n_clicks)
#         if n_clicks is not None and n_clicks > 0:
#             return '/dashboard1'
# #             return 'http://127.0.0.1:8055/'

#     app.layout = html.Div([
#         html.H1('Dashboard 2'),
#         dcc.Graph(
#             id='chart2',
#             figure={
#                 'data': [
#                     go.Scatter(x=x_values2, y=y_values2, mode='lines+markers', name='Data 2')
#                 ],
#                 'layout': go.Layout(
#                     title='Chart 2',
#                     xaxis=dict(title='X-axis Title'),
#                     yaxis=dict(title='Y-axis Title')
#                 )
#             }
#         ),
#         dcc.Location(id='url'),
#         html.Button('Switch to Dashboard 1', id='btn-switch', n_clicks=0)
#     ])

#     app.run_server(debug=False, port=8056)

# if __name__ == '__main__':
#     thread1 = threading.Thread(target=create_dash_app1)
#     thread2 = threading.Thread(target=create_dash_app2)

#     thread1.start()
#     thread2.start()


# In[ ]:


# import dash
# import dash_core_components as dcc
# import dash_html_components as html
# import plotly.graph_objects as go
# import threading

# app1 = dash.Dash()
# app1.layout = html.Div([html.Button('Switch to Dashboard 2', id='btn-switch'), dcc.Graph(id='graph1')])
# @app1.callback(Output('url', 'pathname'),
#                   Input('btn-switch', 'n_clicks'))
# def switch_dashboard(n_clicks):
#     if n_clicks is not None and n_clicks > 0:
#         return '/dashboard2'

# # Specify the port number
# app1.run_server(port=8001)

# app2 = dash.Dash()
# app2.layout = html.Div([html.Button('Switch to Dashboard 1', id='btn-switch'), dcc.Graph(id='graph2')])
# @app2.callback(Output('url', 'pathname'),
#                   Input('btn-switch', 'n_clicks'))
# def switch_dashboard(n_clicks):
#     if n_clicks is not None and n_clicks > 0:
#         return '/dashboard2'

# # Specify the port number
# app2.run_server(port=8002)


# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:




